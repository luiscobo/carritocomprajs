(function () {
    "use-strict";
  
     /* Para que se espere a que toda la pagina(html,css) este cargada */
     document.addEventListener('DOMContentLoaded',function () {       
        // Variables
        // Simulamos la base de datos con LocalStorage, los datos iniciales se cargarán en el fichero baseDatosMock.js
        let productos = JSON.parse(localStorage.getItem('productos'));
        let categorias = JSON.parse(localStorage.getItem('categorias'));
        let carrito = [];
        let total = 0;
        let productosCategoria = [];  
        let DomTablaProductos;
        let DOMSelectCategoria;

       // Elementos DOM
        const DOMcompra= document.querySelector('#compra');       
        const DOMadministrador= document.querySelector('#administrador');
        const DOMnavCategorias= document.querySelector('#nav-categorias');
        const DOMlistaProductos= document.querySelector('#lista-productos');
        const DOMlistaCategoriasAdministrador= document.querySelector('#lista-categorias-administrador');
        const DOMlistaCompra= document.querySelector('#listaCompra');
        const DOMtotal = document.querySelector('#total');
        const DOMcarritoCompra = document.querySelector('#idCarritoCompra');
        const DOMproductosCompra = document.querySelector('#idProductosCompra');
        
        // Botones DOM
        const DOMbtnAdministrar = document.querySelector('#btn-administrar');
        const DOMbtnComprar = document.querySelector('#btn-comprar');
        const DOMbtnNuevaCategoria = document.querySelector('#btnNuevaCategoria');
        const DOMbtnNuevoProducto = document.querySelector('#btnNuevoProducto');       
        const DOMbtnPedido = document.querySelector('#boton-pedido');     
        const DOMbtnCarritoCompra = document.querySelector('#btnCarritoCompra');         
        // EventListener

        DOMbtnAdministrar.addEventListener('click', activarVistaAdministrar);
        DOMbtnNuevaCategoria.addEventListener('click', nuevaCategoria);
        DOMbtnNuevoProducto.addEventListener('click', nuevoProducto);
        DOMbtnComprar.addEventListener('click', activarVistaComprar);
        DOMbtnCarritoCompra.addEventListener('click', activarCarritoCompra); 

        // Carga inicial de elementos
        DOMadministrador.style.display = "none";
        DOMbtnComprar.style.display = "none";
        
        renderizarCategorias();
        renderizarProductos(); 
        renderizarCarrito();
        /******************************************************** FUNCIONES *****************************************************************/

        /**************************************************** Funciones vista compra *******************************************/
        /**
        * Dibuja todos las pestañas con las categorias
        */
        function renderizarCategorias() {
            removeAllChildNodes(DOMnavCategorias);
            categorias.forEach((categoria,index) => {
                let ariaSelected = false; 
                 const nodoNav = document.createElement('a');                
                 if (index==0) {
                    ariaSelected = true;
                    nodoNav.classList.add('active');                    
                 }
                 nodoNav.classList.add('nav-link');
                 nodoNav.setAttribute('id','nav-'+categoria.nombre+'-tab');
                 nodoNav.setAttribute('data-toggle','tab');
                 nodoNav.setAttribute('href','#nav-lista-productos');
                 nodoNav.setAttribute('role','tab');
                 nodoNav.setAttribute('aria-controls','nav-'+categoria.nombre);
                 nodoNav.setAttribute('aria-selected', ariaSelected);
                 nodoNav.setAttribute('categoria', categoria.nombre);
                 nodoNav.textContent = categoria.nombre;

                 DOMnavCategorias.appendChild(nodoNav);
                 nodoNav.addEventListener('click', renderizarProductos);
            });

        };
        /**
        * Dibuja todos los productos a partir de la base de datos.
        */
        function renderizarProductos(evento) {

            productosCategoria = [];
            removeAllChildNodes(DOMlistaProductos);
            let categoria;
            // Obtenemos la categoria activa       
            if (evento == null){
                categoria = categorias[0].nombre;    
            } else {
                categoria = evento.target.getAttribute('categoria');     
            };  
            // Buscamos solo los productos de la categoria activa
            productosCategoria = productos.filter((itemBaseDatos) => {
                return itemBaseDatos.categoria === categoria;
            });
          
            productosCategoria.forEach((info) => {
                // Estructura
                const miNodo = document.createElement('div');
                miNodo.classList.add('card', 'col-sm-4', 'text-center');
                // Body
                const nodoCardBody = document.createElement('div');
                nodoCardBody.classList.add('card-body');
                // Nombre
                const nodoNombre = document.createElement('h5');
                nodoNombre.classList.add('card-title');
                nodoNombre.style.fontWeight = 'bold';
                nodoNombre.textContent = info.nombre;
                // Imagen
                const nodoImagen = document.createElement('img');
                if(info.stock<=0) {
                    nodoImagen.classList.add('img-thumbnail','oscurecer-img');
                } else {
                    nodoImagen.classList.add('img-thumbnail','zoom',);
                }                
                nodoImagen.setAttribute('id','imgIdP'+info.id);
                nodoImagen.setAttribute('src', 'data:image/jpeg;base64,'+info.imagen);
                // codigo
                const nodoCodigo = document.createElement('p');
                nodoCodigo.classList.add('card-text');
                nodoCodigo.textContent = 'CDO'+info.id;
                // Descripcion
                const nodoDescripcion = document.createElement('p');
                nodoDescripcion.classList.add('card-text');
                nodoDescripcion.textContent = info.descripcion;
                // Precio
                const nodoPrecio = document.createElement('p');
                nodoPrecio.classList.add('card-text');
                nodoPrecio.style.fontWeight = 'bold';
                nodoPrecio.textContent = info.precio + '€';
                // Stock
                const nodoStock = document.createElement('p');
                nodoStock.setAttribute('id','stockId'+info.id);
                nodoStock.classList.add('card-text');
                nodoStock.textContent = info.stock + ' disponibles';
                // Boton 
                const nodoAgregarCarrito = document.createElement('button');
                nodoAgregarCarrito.classList.add('btn', 'btn-outline-primary');
                nodoAgregarCarrito.textContent = 'Agregar al carrito';
                nodoAgregarCarrito.setAttribute('idProducto', info.id);
                nodoAgregarCarrito.setAttribute('id', 'btnId'+info.id);
                if(info.stock<=0) {
                    nodoAgregarCarrito.setAttribute('disabled', true);
                    nodoAgregarCarrito.setAttribute('aria-disabled', true);
                }
                nodoAgregarCarrito.addEventListener('click', agregarProductoAlCarrito);
                // Insertamos
                nodoCardBody.appendChild(nodoImagen);
                nodoCardBody.appendChild(nodoNombre);
                nodoCardBody.appendChild(nodoCodigo);
                nodoCardBody.appendChild(nodoDescripcion);
                nodoCardBody.appendChild(nodoPrecio);                
                nodoCardBody.appendChild(nodoStock);  
                nodoCardBody.appendChild(nodoAgregarCarrito);
                miNodo.appendChild(nodoCardBody);
                DOMlistaProductos.appendChild(miNodo);
            });
        }    
        /**
        * Dibuja todos los productos añadidos en el carrito
        */
        function renderizarCarrito() { 
            // Vaciamos todo el html
            removeAllChildNodes(DOMlistaCompra);
            // Quitamos los duplicados
            const carritoSinDuplicados = [...new Set(carrito)];
            // Generamos los Nodos a partir de carrito
            carritoSinDuplicados.forEach((item) => {
                // Obtenemos el item que necesitamos de la variable base de datos
                const miItem = productos.filter((itemBaseDatos) => {
                    // ¿Coincide las id? Solo puede existir un caso
                    return itemBaseDatos.id === parseInt(item);
                });
                // Cuenta el número de veces que se repite el producto
                const cantidadItem = carrito.reduce((total, itemId) => {
                    // ¿Coincide las id? Incremento el contador, en caso contrario no mantengo
                    return itemId === item ? total += 1 : total;
                }, 0);
                // Creamos el nodo del item del carrito
                const nodoLista = document.createElement('li');
                nodoLista.classList.add('row', 'prouducto-carrito', 'border-bottom', 'pt-3','ml-0');
                //miNodo.textContent = `${numeroUnidadesItem} x ${miItem[0].nombre} - ${miItem[0].precio}€`;

                // Creamos el nodo div e imagen
                const nodoDivImagenCarrito = document.createElement('div');
                nodoDivImagenCarrito.classList.add('col-3', 'p-0');
                const nodoImagenCarrito = document.createElement('img');
                nodoImagenCarrito.setAttribute('src', 'data:image/jpeg;base64,'+miItem[0].imagen);
                nodoDivImagenCarrito.appendChild(nodoImagenCarrito);
                // Creamos div para propiedades productos                  
                const nodoPropiedades = document.createElement('div');
                nodoPropiedades.classList.add('col-4', 'p-0');    
                // nombre
                const nodoNombreCarrito = document.createElement('h6');
                nodoNombreCarrito.classList.add('m-0');
                nodoNombreCarrito.style.fontWeight = 'bold';
                nodoNombreCarrito.textContent = miItem[0].nombre;
                nodoPropiedades.appendChild(nodoNombreCarrito);
                // precio
                const nodoPrecioCarrito = document.createElement('p');
                nodoPrecioCarrito.classList.add('m-0');
                nodoPrecioCarrito.textContent = (cantidadItem * miItem[0].precio) + '€';
                nodoPropiedades.appendChild(nodoPrecioCarrito);
                // codigo
                const nodoCodigoCarrito = document.createElement('p');
                nodoCodigoCarrito.classList.add('m-0');
                nodoCodigoCarrito.textContent = 'COD'+miItem[0].id;
                nodoPropiedades.appendChild(nodoCodigoCarrito);
                // Cantidad                  
                const nodoCantidadCarrito = document.createElement('div');
                nodoCantidadCarrito.classList.add('col-3', 'p-0','text-center'); 
                nodoCantidadCarrito.textContent = cantidadItem;
                // Boton de eliminar carrito
                const botonEliminar = document.createElement('a');
                botonEliminar.classList.add('col-2','bi', 'bi-trash-fill','text-right');               
                botonEliminar.style.fontSize = '1.3rem';
                botonEliminar.style.color = '#9E2B12';
                botonEliminar.role='button';
                botonEliminar.setAttribute('idProducto', miItem[0].id);
                botonEliminar.addEventListener('click', borrarProductoCarrito);
                // añadimos los nodos
                nodoLista.appendChild(nodoDivImagenCarrito);
                nodoLista.appendChild(nodoPropiedades);   
                nodoLista.appendChild(nodoCantidadCarrito);             
                nodoLista.appendChild(botonEliminar);
                DOMlistaCompra.appendChild(nodoLista);
            });
            // comprobamos si no tenemos productos en el carrito para no habilitar el boton de compra.
            if (carrito.length==0){
                DOMbtnPedido.setAttribute('disabled', true);
                DOMbtnPedido.setAttribute('aria-disabled', true);
            } else {
                DOMbtnPedido.removeAttribute('disabled');
                DOMbtnPedido.setAttribute('aria-disabled', false);
            }
        }
        /**
        * Calcula el precio total de los elementos añadidos al carrito
        */
        function calcularTotal() {           
            // Limpiamos precio anterior
            total = 0;
            // Recorremos el array del carrito
            carrito.forEach((item) => {
                // De cada elemento obtenemos su precio
                const miItem = productos.filter((itemBaseDatos) => {
                    return itemBaseDatos.id === parseInt(item);
                });               
                total = total + miItem[0].precio;
            });
            // Renderizamos el precio en el HTML
            DOMtotal.textContent = total.toFixed(2) + '€';
        }
        /**
        * Permite añadir un producto al carrito
        */
        function agregarProductoAlCarrito(evento) {
            const id = evento.target.getAttribute('idProducto');
            // Anyadimos el Nodo a nuestro carrito
            carrito.push(id);           
            productos.forEach(function(item){
                if(item.id==id && item.stock>0){
                  item.stock = item.stock-1;
                  //Actualizamos el stock del producto
                  const nStock= document.querySelector('#stockId'+id);
                  nStock.textContent = item.stock+' Disponibles';          
                }             
                
                if (item.id==id && item.stock==0){
                  //Deshabilitamos la compra del producto  
                  const nImg= document.querySelector('#imgIdP'+id);
                  nImg.classList.add('oscurecer-img');
                  nImg.classList.remove('zoom');
                  console.log(nImg);
                  const nBtn= document.querySelector('#btnId'+id);
                  nBtn.setAttribute('disabled', true);
                  nBtn.setAttribute('aria-disabled', true);
                }
                
                return item;
            });
            // Calculo el total
            calcularTotal();
            // Actualizamos el carrito 
            renderizarCarrito();
        }
        /**
        * Permite borrar un producto al carrito
        */
        function borrarProductoCarrito(evento) {
           // Obtenemos el producto ID que hay en el boton pulsado
           const id = evento.target.getAttribute('idProducto');  
           // Buscamos el primer elemento del array que sea igual al id        
           let index = carrito.findIndex(item => item == id);
           // Eliminamos el elemento
           carrito.splice(index,1);
           // Añadimos de nuevo la unidad al Stock del producto
           productos.forEach(function(item){
            if(item.id==id){
              item.stock = item.stock+1;
              //Actualizamos el html
              const nStock= document.querySelector('#stockId'+id);
              nStock.textContent = item.stock+' Disponibles';                  
            }
            if(item.stock>0){
              //Habilitamos la compra del producto
              const nImg= document.querySelector('#imgIdP'+id);
              nImg.classList.add('zoom');
              nImg.classList.remove('oscurecer-img');
              const nBtn= document.querySelector('#btnId'+id);
              nBtn.removeAttribute('disabled');
              nBtn.setAttribute('aria-disabled', false);
            }            
            return item;
        });
           // volvemos a renderizar
           renderizarCarrito();
           // Calculamos de nuevo el precio
           calcularTotal();
        }
        /**
        * Permite activar el carrito de compra desde la vista de un dispositivo movil
        */
        function activarCarritoCompra() {     

            //DOMcarritoCompra.classList.add('col-md-12');
            DOMcarritoCompra.classList.remove('d-none', 'd-md-block');
            DOMproductosCompra.classList.add('d-none', 'd-md-block');
            DOMbtnCarritoCompra.classList.remove('bi-bag-check-fill');
            DOMbtnCarritoCompra.classList.add('bi-backspace-fill');

        }      
      
        /************************************************** Funciones vista compra ***************************************************************/
       
        /**
        * Dibuja la lista de las categorias.
        */
        function renderizarCategoriasAdministradro() {
            removeAllChildNodes(DOMlistaCategoriasAdministrador);
            categorias.forEach((item) => {
                // Estructura
                const nodoItemLista = document.createElement('li');
                nodoItemLista.textContent = item.nombre;
                DOMlistaCategoriasAdministrador.appendChild(nodoItemLista);
            });
        } 

        /**
        * Dibuja una tabla con todos los productos.
        */
          function renderizarProductosAdministrador() {
            // Vaciamos todo el html
            removeAllChildNodes(DomTablaProductos);
            productos.forEach((info) => {   
                // Estructura
                const nodoRegistro= document.createElement('tr');
                // codigo
                const nodoCodigo = document.createElement('td');
                nodoCodigo.textContent = 'COD'+info.id;
                // Nombre
                const nodoNombre = document.createElement('td');
                nodoNombre.textContent = info.nombre;                
                // Descripcion
                const nodoDescripcion = document.createElement('td');
                nodoDescripcion.textContent = info.descripcion;
                // categoria
                 const nodoCategoria = document.createElement('td');
                 nodoCategoria.textContent = info.categoria;
                // Precio
                const nodoPrecio = document.createElement('td');
                nodoPrecio.textContent = info.precio + '€';
                // Stock
                const nodoStock = document.createElement('td');
                nodoStock.textContent = info.stock;
                //imagen
                const nodoImagen = document.createElement('img');
                nodoImagen.classList.add('imagen-grid',);
                nodoImagen.setAttribute('id','imgId'+info.id);
                nodoImagen.setAttribute('src', 'data:image/jpeg;base64,'+info.imagen);
                // añadimos los nodos
                nodoRegistro.appendChild(nodoCodigo);
                nodoRegistro.appendChild(nodoNombre);              
                nodoRegistro.appendChild(nodoDescripcion);
                nodoRegistro.appendChild(nodoCategoria);
                nodoRegistro.appendChild(nodoPrecio);                
                nodoRegistro.appendChild(nodoStock);  
                nodoRegistro.appendChild(nodoImagen); 

                DomTablaProductos.appendChild(nodoRegistro);
            });
        }
        /**
        * Activa un modal con el formulario para añadir nueva categoria.
        */
        function nuevaCategoria() {            
            const DOMbtnGuardaCategoria = document.querySelector('#btnGuardarCategoria');  
            DOMbtnGuardaCategoria.addEventListener('click', guardarCategoria); 
            document.querySelector('#nombreCategoria').value = "";
        }
        /**
        * Activa un modal con el formulario para añadir nuevo producto.
        */
        function nuevoProducto() { 
            DOMSelectCategoria = document.querySelector('#selectCategoria');  
            removeAllChildNodes(DOMSelectCategoria); 
            // limpiamos los valores del formulario.            
            document.querySelector('#nombreProducto').value = "";      
            document.querySelector('#descripcionProducto').value = ""; 
            document.querySelector('#stockProducto').value = "";   
            document.querySelector('#precioProducto').value = "";
            // generamos un dropdown con las categorias
            const nodoCategoria = document.createElement('select');
            nodoCategoria.classList.add('form-select');
            nodoCategoria.setAttribute('id','idSelectCategoria');
            categorias.forEach((item) => {
                const nodoItemLista = document.createElement('option');
                nodoItemLista.textContent = item.nombre;
                nodoCategoria.appendChild(nodoItemLista);
            });
            DOMSelectCategoria.appendChild(nodoCategoria);
            const DOMbtnGuardaProducto = document.querySelector('#btnGuardarProducto');  
            DOMbtnGuardaProducto.addEventListener('click', guardarProducto);
        }
        /**
        * guarda la categoria en la variable y la persiste en localStorage.
        */
        function guardarCategoria() {   
            const formCategoria = document.querySelector('#formNuevaCategoria');
            // validamos el formulario
            let valid_form = false;            
            if (formCategoria.checkValidity() !== false ) {
                    valid_form = true;
                }
            formCategoria.classList.add('was-validated');
            if ( valid_form ) {
                categorias.push(
                    {
                       nombre: document.querySelector('#nombreCategoria').value}
                    );
                renderizarCategoriasAdministradro();    
                closeModal('modalNuevaCategoria');
                localStorage.setItem('categorias',JSON.stringify(categorias));
            }else{  
                console.log('invalid ');
            }; 
        }
        /**
        * guarda el producto en la variable y la persiste en localStorage.
        */
        function guardarProducto() {
            const formProducto = document.querySelector('#formNuevoProducto');
            let valid_form = false;
            if ( formProducto.checkValidity() !== false ) {
                valid_form = true;
            }
            formProducto.classList.add('was-validated');
            if ( valid_form ) {
                let nuevoProducto = {    
                    id: productos.length+1,
                    nombre: document.querySelector('#nombreProducto').value,               
                    descripcion: document.querySelector('#descripcionProducto').value,
                    stock: document.querySelector('#stockProducto').value,
                    precio: parseFloat(document.querySelector('#precioProducto').value),
                    imagen: null,
                    categoria: document.querySelector('#idSelectCategoria').value,
               }
                const imagen = document.querySelector('#inputImagen').files[0];                
                // encode the file using the FileReader API
                const reader = new FileReader();    
                if(imagen!=null){                
                    reader.readAsDataURL(imagen); 
                    reader.onloadend = () => {
                        // use a regex to remove data url part
                        const base64String = reader.result
                        .replace("data:", "")
                        .replace(/^.+,/, "");    
                        nuevoProducto.imagen = base64String;
                        productos.push(nuevoProducto); 
                        localStorage.setItem('productos',JSON.stringify(productos));
                        renderizarProductosAdministrador();                
                    };
                };
                closeModal('modalNuevoProducto');
                document.querySelector('#inputImagen').value="";
                console.log('guardar ');                        
                    
            }else{  
                console.log('invalid ');
            };  
        }
        /************************************************** Funciones genericas ***************************************************************/
        function activarVistaComprar() {
            //Ocultamos la visualizacion de productos y carrito
            DOMcompra.style.display = "";   
            DOMadministrador.style.display = "none"; 
            DOMbtnAdministrar.style.display="";  
            DOMbtnComprar.style.display="none";    
            renderizarProductos();
            renderizarCarrito();
            renderizarCategorias();
        }
        function activarVistaAdministrar() {
            //Ocultamos la visualizacion de productos y carrito
            DOMcompra.style.display = "none";   
            DOMadministrador.style.display = "";
            DOMbtnAdministrar.style.display="none";  
            DOMbtnComprar.style.display=""; 

            DOMbtnAdministrar.classList.remove('d-none', 'd-md-block');
            
            DomTablaProductos = document.querySelector('#tablaProductos');
            renderizarProductosAdministrador();
            renderizarCategoriasAdministradro();
        }

        //Limpia un nodo, eliminando los nodos hijos
        function removeAllChildNodes(parent) {
            while (parent.firstChild) {
                parent.removeChild(parent.firstChild);
            }
        }

        function closeModal(idModal) {       
            document.getElementById(idModal).classList.remove("show");
        }
     }); 
   }
 )();